// Generated code from Butter Knife. Do not modify!
package com.example.marcushill.tictac.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivityFragment$$ViewBinder<T extends com.example.marcushill.tictac.fragment.MainActivityFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492983, "field 'recyclerView'");
    target.recyclerView = finder.castView(view, 2131492983, "field 'recyclerView'");
  }

  @Override public void unbind(T target) {
    target.recyclerView = null;
  }
}
