package com.example.marcushill.tictrac.database;


/**
 * Created by Marcus Hill on 07/03/2016.
 */
public class UsersTable {

    public final static String USERS_TABLE = "users";

    public final static String EMAIL = "email";
    public final static String NAME = "name";
    public final static String INFOS = "infos";
    private static final String TAG = UsersTable.class.getSimpleName();

    protected static String createTable(){
        return "CREATE TABLE " + USERS_TABLE + "(" + EMAIL + " NOT NULL UNIQUE, " +
                NAME + " TEXT NOT NULL, " + INFOS + " TEXT NOT NULL)";
    }



}
