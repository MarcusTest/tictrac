package com.example.marcushill.tictrac.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.marcushill.tictrac.Constants;
import com.example.marcushill.tictrac.R;
import com.example.marcushill.tictrac.Utils;
import com.example.marcushill.tictrac.database.DBHelper;
import com.example.marcushill.tictrac.database.UsersTable;
import com.example.marcushill.tictrac.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Marcus Hill on 07/03/2016.
 */
public class GetUsersService extends IntentService{
    private static final String TAG = GetUsersService.class.getSimpleName() ;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GetUsersService(String name) {
        super(name);
    }

    private final OkHttpClient client = new OkHttpClient();

    public GetUsersService(){
        super("GetUsersService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(Utils.checkConnectivity(this)){
            Log.i(TAG,"Has Connectivity");

            Request request = new Request.Builder()
                    .url(getString(R.string.url))
                    .build();

                Response response = null;
                try {

                    response = client.newCall(request).execute();
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    String json = response.body().string();

                    JSONObject jsonObject= new JSONObject(json);
                    JSONArray jsonArray = jsonObject.getJSONArray("users");
                    List<User> users = new ArrayList<User>();
                    User user = null;
                    for(int i = 0; i < jsonArray.length(); i++){
                        JSONObject userJson = (jsonArray.getJSONObject(i));
                        user = new User();
                        user.setEmail(userJson.getString("email"));
                        user.setName(userJson.getString("name"));
                        user.setInfos(userJson.getString("infos"));
                        users.add(user);
                    }

                    SQLiteDatabase db  = new DBHelper(this).getWritableDatabase();
                    ContentValues values = null;
                    if(users != null){
                        for(User u: users) {

                            values = new ContentValues();
                            values.put(UsersTable.EMAIL, u.getEmail());
                            values.put(UsersTable.NAME, u.getName());
                            values.put(UsersTable.INFOS, u.getInfos());
                            db.insertWithOnConflict(UsersTable.USERS_TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);

                        }
                    }
                    else{
                        Log.d(TAG, "Users is null");
                    }

                    Intent broadcast = new Intent();
                    broadcast.setAction(Constants.USER_DATA_CHANGE);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(broadcast);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }
    }
}
