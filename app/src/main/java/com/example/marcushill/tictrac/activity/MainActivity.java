package com.example.marcushill.tictrac.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.marcushill.tictrac.Constants;
import com.example.marcushill.tictrac.R;
import com.example.marcushill.tictrac.database.DBHelper;
import com.example.marcushill.tictrac.database.UsersTable;
import com.example.marcushill.tictrac.fragment.MainActivityFragment;
import com.example.marcushill.tictrac.model.User;
import com.example.marcushill.tictrac.services.GetUsersService;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {


    MainActivityFragment mainActivityFragment;
    private OnDataChangedListener onDataSetChangedListner;

    private String TAG = MainActivity.class.getSimpleName();
    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "Intent received");
            SQLiteDatabase db = new DBHelper(context).getReadableDatabase();
            Cursor cursor = db.query(UsersTable.USERS_TABLE, null, null, null, null, null, null);
            onDataSetChangedListner.onDataChanged(cursor);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mainActivityFragment = (MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        mainActivityFragment.setOnSetClickListener(new MainActivityFragment.OnSetClickListener() {
            @Override
            public void onClick(User user) {
                getSupportActionBar().setTitle(user.getName());
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(Constants.USER_DATA_CHANGE));
        Log.d(TAG, "Broadcast Receiver regidtered");
        startService(new Intent(this, GetUsersService.class));

    }

    @Override
    protected void onPause() {
        // Unregister since the activity is not visible
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setOnDataChangedListener(OnDataChangedListener onDataSetChangedListner){
        this.onDataSetChangedListner = onDataSetChangedListner;
    }

   public interface OnDataChangedListener{
       void onDataChanged(Cursor cursor);
   }


}
