package com.example.marcushill.tictrac.fragment;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.marcushill.tictrac.R;
import com.example.marcushill.tictrac.activity.MainActivity;
import com.example.marcushill.tictrac.database.UsersTable;
import com.example.marcushill.tictrac.model.User;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    @Bind(R.id.recycle_view) RecyclerView recyclerView;
    private OnSetClickListener onSetClickListener;
    private CardAdapter adapter;

    public MainActivityFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new CardAdapter(getContext(),null);
        recyclerView.setAdapter(adapter);
        MainActivity activity = (MainActivity) getActivity();
        activity.setOnDataChangedListener(new MainActivity.OnDataChangedListener() {
            @Override
            public void onDataChanged(Cursor cursor) {
                adapter.swapCursor(cursor);
                adapter.notifyDataSetChanged();
            }
        });

    }

    public void setOnSetClickListsener(OnSetClickListener onsetClickListener){
        this.onSetClickListener = onsetClickListener;
    }


    public class CardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Cursor cursor;
        private Context context;

        public CardAdapter(Context context, Cursor c){
            this.context = context;
            this.cursor = c;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_user, parent, false);
            CardHolder cv = new CardHolder(v);
            return cv;

        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(cursor != null) {
                cursor.moveToPosition(position);
                CardHolder cardHolder = (CardHolder) holder;
                cardHolder.email.setText(cursor.getString(cursor.getColumnIndex(UsersTable.EMAIL)));
                cardHolder.name.setText(cursor.getString(cursor.getColumnIndex(UsersTable.NAME)));
                cardHolder.infos = cursor.getString(cursor.getColumnIndex(UsersTable.INFOS));
            }

        }

        public void swapCursor(Cursor cursor){
            this.cursor = cursor;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            if(cursor==null) return 0;
            return cursor.getCount();
        }

        public class CardHolder  extends RecyclerView.ViewHolder{

            public TextView name;
            public TextView email;
            public Button setButton;
            String infos;

            public CardHolder(android.view.View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.user_name);
                email = (TextView) itemView.findViewById(R.id.user_email);
                setButton = (Button) itemView.findViewById(R.id.set_button);
                setButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(onSetClickListener!=null){
                            User user = new User();
                            user.setEmail(email.getText().toString());
                            user.setName(name.getText().toString());
                            user.setInfos(infos);
                            onSetClickListener.onClick(user);
                        }
                    }
                });
            }


        }


    }

    public void setOnSetClickListener(OnSetClickListener onSetClickListener){
        this.onSetClickListener = onSetClickListener;
    }
    public interface OnSetClickListener{
        void onClick(User user);
    }


}


