package com.example.marcushill.tictrac.model;

/**
 * Created by Marcus Hill on 07/03/2016.
 */
public class User {

    private String email, name, infos;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }


}
